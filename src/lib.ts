import * as crypto from 'crypto';

const MAX_RANDOM: number = 4096;
const RANDOM_SET_SIZE: number = 8;

const registry: { [key: number]: string } = {};
let addresses: number[];

/*
  Whenever a new memory server is registered, try and claim a good distribution
  of "addresses" in the hash ring.  This relies heavily on the uniformity of
  random values.  If this distribution is non-uniform, a very small number of
  servers will end up with a large number of stored values.
*/

export function register (ip: string): void {
  for (let i = 0; i < RANDOM_SET_SIZE; ++i) {
    const rand = Math.floor(Math.random() * MAX_RANDOM);
    if (registry[rand]) continue;

    registry[rand] = ip;
  }
  addresses = Object.keys(registry).map(k => parseInt(k));
  addresses.sort((a, b) => a - b);
}

/*
  Locate the nearest available memory server that maintains an address "above"
  the hash key (modulo the number of possible hash values).
*/

export function findServer (key: string): { ip: string, hash: string } {
  if (!addresses.length) return undefined;

  const hash: string = crypto.createHash('md5').update(key).digest('hex');
  const value: number = parseInt(hash.substring(hash.length - 3), 16);

  let left = 0;
  let right = addresses.length - 1;

  while (left < right) {
    const mid = Math.floor((left + right) / 2);
    if (value === addresses[mid]) {
      left = mid;
      break;
    }
    else if (value < addresses[mid]) right = mid - 1;
    else left = mid + 1;
  }

  left >= addresses.length ? 0 : left;
  const ip: string = registry[addresses[left]];

  return { ip, hash };
}
