import * as restify from 'restify';
import * as http from 'http';

const options = {
  hostname: 'controller',
  port: parseInt(process.env.CONTROLLER_PORT || "8080"),
  path: '/register',
  method: 'POST',
};

http.request(options, res => {
  if (res.statusCode === 200)
    console.log('Successfully registered with controller.');
  res.on('error', err => console.log(`Error registering with controller: ${err.message}`));
}).end();

const port: number = parseInt(process.env.HTTP_PORT || "80");
const server = restify.createServer();

/*
  TODO: In-memory cache is extremely volatile.  Given that these servers can go
  down at any time -- taking all their keys with them -- it is a good idea to
  explore some stable storage here to be flushed to on a schedule.  At least
  then, services could come and go -- loading values from stable storage whenever
  there is a cache miss.
*/

const cache: { [key: string]: string } = {};

server.get('/:key', (req, res) => {
  if (!cache[req.params.key]) res.statusCode = 404;
  console.log(`Reading cache at ${req.params.key}`);
  res.end(cache[req.params.key]);
});

server.put('/:key', (req, res) => {
  let value = '';
  req.on('data', d => value += d);
  req.on('end', () => {
    cache[req.params.key] = value;
    console.log(`Wrote to cache at ${req.params.key}`);
    res.end();
  });
});

server.del('/:key', (req, res) => {
  delete cache[req.params.key];
  console.log(`Deleted cache at ${req.params.key}`);
  res.end();
});

server.listen(port, () => console.log(`Server started: ${server.url}`));

/* Network socket clean up. */

process.once('SIGINT', () => server.close());
process.once('SIGTERM', () => server.close());
