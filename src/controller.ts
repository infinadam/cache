import * as restify from 'restify';
import * as http from 'http';
import { register, findServer } from './lib';

/*
  Private registration server.

  TODO: better to use web sockets here.  However, not enough time to come up
  with a simple communications protocol.  HTTP will do for now, but if a memory
  server goes down, requests for its keys will start to time out...
*/
const internalPort: number = parseInt(process.env.INTERNAL_HTTP_PORT || '8080');
const internalServer: restify.Server = restify.createServer();

internalServer.post('/register', (req, res, next) => {
  const ip = req.connection.remoteAddress.split(':').pop();
  register(ip);
  res.end();
});

internalServer.listen(internalPort, () => {
  console.log(`Accepting memory workers at: ${internalServer.url}`);
});

/* Public-facing store and retrieve server. */

const port: number = parseInt(process.env.HTTP_PORT || '80');
const server: restify.Server = restify.createServer();

server.get('/:key', (req, res) => {
  const { ip, hash } = findServer(req.params.key);
  console.log(`Reading ${hash} from ${ip}.`);
  const options = {
    hostname: ip,
    path: `/${hash}`,
  };
  http.request(options, r => {
    res.statusCode = r.statusCode;
    let value = '';
    r.on('data', d => value += d);
    r.on('end', () => res.send(value));
  }).end();
});

server.put('/:key', (req, res) => {
  const { ip, hash } = findServer(req.params.key);
  console.log(`Writing ${hash} to ${ip}.`);

  const options = {
    hostname: ip,
    path: `/${hash}`,
    method: 'PUT'
  };

  let value = '';
  req.on('data', d => value += d);
  req.on('end', () => {
    http.request(options, r => {
      res.statusCode = r.statusCode;
      res.send();
    }).end(value);
  });
});

server.del('/:key', (req, res) => {
  const { ip, hash } = findServer(req.params.key);
  console.log(`Deleting ${hash} from ${ip}.`);

  const options = {
    hostname: ip,
    path: `/${hash}`,
    method: 'DELETE',
  };

  http.request(options, r => {
    res.statusCode = r.statusCode;
    res.send();
  }).end();
});

server.listen(port, () => console.log(`Server started: ${server.url}`));

/* Network socket clean up. */

function close () {
  internalServer.close();
  server.close();
}

process.once('SIGINT', close);
process.once('SIGTERM', close);
