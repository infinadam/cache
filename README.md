# About

A simple, distributed caching service.

You can add keys as follows:

```
$ curl -XPUT http://localhost:9990/<key> -d "<data>"
```

Retrieval of keys is a simple get:

```
$ curl http://localhost:9990/<key>
```

Deleting a key may be accomplished by:

```
$ curl -XDELETE http://localhost:9990/<key>
```

# Install

```
$ npm install
```

# Build

```
$ npm run build
```

# Run

```
$ docker-compose up -d --scale memory=<n>
```
Where `n` is the desired number of memory servers.

# Clean

```
$ npm run clean
```
